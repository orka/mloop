# Nested Loops v. Single multi loop test
Simple test to determine 0(n) of nested loops vs single multidimensional loop performance.

While running function T times to determine the average time with N size of input
We can observe the following:

### Functions
Nested:
```js
for (let i = 0; i < n; i++) {
    for (let j = 0; j < n; j++) {
        total += j * i
    }
}
```

Single:
```js
for (let i = 0,
    j = 0;
    i < n;
    i += j === n - 1 ? 1 : 0,
    j += j < n - 1 ? 1 : -j
) {
    total += j * i
}
```

## Conclusion 

It appears that nested multidimensional loops are about double the efficiency than a single loop definition, where:
nested: 0(n^2)
single: 0(n^2)