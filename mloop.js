

function nested(n, t) {
    const times = []

    while (t-- > 0) {
        const start = performance.now()
        let total = 0;
        for (let i = 0; i < n; i++) {
            for (let j = 0; j < n; j++) {
                total += j * i
            }
        }
        times.push(performance.now() - start)
    }
    return times.reduce((c, t) => c + t) / times.length
}

function single(n, t) {
    const times = []

    while (t-- > 0) {
        const start = performance.now()
        let total = 0;
        for (let i = 0,
            j = 0;
            i < n;
            i += j === n - 1 ? 1 : 0,
            j += j < n - 1 ? 1 : -j
        ) {
            total += j * i
        }
        times.push(performance.now() - start)
    }
    return times.reduce((c, t) => c + t) / times.length
}

function test(n, t) {
    const res = {
        single: single(n, t),
        nested: nested(n, t)
    }
    return res
}

function mloopTest(event) {
    event.preventDefault();
    const value = Number(event.target.elements.input.value);
    const runTimes = Number(event.target.elements.times.value);
    const res = test(value, runTimes)
    const text = `Test results: 
    Nested for loop avg time: ${res.nested} ms
    Single for loop avg time: ${res.single} ms
    `
    return text
}
